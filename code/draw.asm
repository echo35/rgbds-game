mov: MACRO
	ld	a, \2
	ld	\1, a
ENDM
draw_MainMenu: ; メインメニューを表示
	call	screen_reset
	;                xx    yy
	mov	[$9800+1*04+32*02], $1A ; は
	mov	[$9800+1*05+32*02], $0C ; し
	mov	[$9800+1*05+32*01], $35 ; 丸点
	mov	[$9800+1*06+32*02], $22 ; め
	mov	[$9800+1*07+32*02], $29 ; る

	mov	[$9800+1*04+32*04], $0E ; せ
	mov	[$9800+1*05+32*04], $2F ; っ
	mov	[$9800+1*06+32*04], $13 ; て
	mov	[$9800+1*07+32*04], $02 ; い

	mov	[$FE00], 16+8*2 ; Y座標
	mov	[$FE01], 8+8*3 ; X座標
	mov	[$FE02], $3C ; 矢印
	mov	[$FE03], 0
	ld	a, [menu_sentaku]
._a:
	cp	0
	jr	z, ._a_end
	push	af
	ld	a, [$FE00]
	add	16
	ld	[$FE00], a
	pop	af
	dec	a
	jr	._a
._a_end:

	call	lcd_on
.loop:
	nop
	call	wait_vblank
	call	get_btn
	ld	a, b
	and	%00001000
	jr	z, .start
	ld	a, b
	and	%00000100
	jr	z, .select
	ld	a, b
	and	%01000000
	jr	z, .up
	ld	a, b
	and	a, %10000000
	jr	z, .down
	ld	a, b
	and	a, %00000001
	jr	z, .a
	jr	.loop
.up:
	ld	a, [$FE00]
	cp	16+8*2
	jp	z, .end_loop
	ld	a, [$FE00]
	sub	a, 16
	ld	[$FE00], a
	ld	a, [menu_sentaku]
	dec	a
	ld	[menu_sentaku], a
	jr	.end_loop
.down:
	ld	a, [$FE00]
	cp	16+8*4
	jp	z, .end_loop
	ld	a, [$FE00]
	add	a, 16
	ld	[$FE00], a
	ld	a, [menu_sentaku]
	inc	a
	ld	[menu_sentaku], a
	jr	.end_loop
.start:
	jp	draw_Pong
	nop
.select:
	jp	draw_Settings
.a:
	ld	a, [menu_sentaku]
	cp	0
	jp	z, .start
	ld	a, [menu_sentaku]
	cp	1
	jp	z, draw_Settings
.end_loop:
	call	timer
	jr	.loop

draw_Settings:
	call	screen_reset
	;                xx    yy
	mov	[$9800+1*04+32*02], $23 ; も
	mov	[$9800+1*05+32*02], $14 ; と
	mov	[$9800+1*05+32*01], $35 ; 丸点
	mov	[$9800+1*06+32*02], $29 ; る

	mov	[$FE00], 16+8*2 ; Y座標
	mov	[$FE01], 8+8*3 ; X座標
	mov	[$FE02], $3C ; 矢印
	mov	[$FE03], 0

	call	lcd_on
.loop:
	nop
	call	wait_vblank
	call	get_btn
	ld	a, b
	and	%01000000
	jr	z, .up
	ld	a, b
	and	a, %10000000
	jr	z, .down
	ld	a, b
	and	a, %00000001
	jr	z, .a
	ld	a, b
	and	a, %00000010
	jr	z, .b

	jr	.loop
.up:
	ld	a, [$FE00]
	cp	16+8*2
	jp	z, .end_loop
	ld	a, [$FE00]
	sub	a, 16
	ld	[$FE00], a
	ld	a, [menu_sentaku]
	dec	a
	ld	[menu_sentaku], a
	jr	.end_loop
.down:
	ld	a, [$FE00]
	cp	16+8*2
	jp	z, .end_loop
	ld	a, [$FE00]
	add	a, 16
	ld	[$FE00], a
	ld	a, [menu_sentaku]
	inc	a
	ld	[menu_sentaku], a
	jr	.end_loop
.a:
.b:
	jp	draw_MainMenu
.end_loop:
	call	timer
	jr	.loop

draw_Pong:
	call	screen_reset
	mov	[$FE00], 16+8*2 ; Y座標
	mov	[$FE01], 8+8*3 ; X座標
	mov	[$FE02], $01 ; 「あ」
	mov	[$FE03], 0
	call	lcd_on
.loop:
	nop
	call	wait_vblank
	call	get_btn
	ld	a, b
	and	%01000000
	jr	z, .up
	ld	a, b
	and	a, %10000000
	jr	z, .down

	jr	.loop
.up:
.down:
	halt
